import React, { Component } from "react";
import Highcharts from "highcharts";
import {
  HighchartsChart,
  Chart,
  withHighcharts,
  XAxis,
  YAxis,
  Title,
  Legend,
  ColumnSeries,
  SplineSeries,
  PieSeries
} from "react-jsx-highcharts";

const headingStyle = {
  "text-align": "center"
};

class LineChart extends Component {
  render() {
    const pieData = [
      {
        name: "Overdue",
        y: 13
      },
      {
        name: "Due",
        y: 23
      },
      {
        name: "Paid",
        y: 19
      }
    ];

    return (
      <div>
        <h1 style={headingStyle}> Income</h1>
        <div className="app">
          <HighchartsChart>
            <Chart />

            <Legend />

            <XAxis
              categories={["June", "July", "August", "September", "October"]}
            />

            <YAxis>
              <ColumnSeries name="Overdue" data={[3, 2, 1, 3, 4]} />
              <ColumnSeries name="Due" data={[2, 3, 5, 7, 6]} />
              <ColumnSeries name="Paid" data={[4, 3, 3, 9, 0]} />
              <SplineSeries name="Average" data={[3, 2.67, 3, 6.33, 3.33]} />
              <PieSeries
                name="Total consumption"
                data={pieData}
                center={[100, 80]}
                size={100}
                showInLegend={false}
              />
            </YAxis>
          </HighchartsChart>
        </div>
      </div>
    );
  }
}

export default withHighcharts(LineChart, Highcharts);
