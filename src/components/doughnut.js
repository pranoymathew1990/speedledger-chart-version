import React, { Component } from "react";
import { Doughnut } from "react-chartjs-2";

const headingStyle = {
  "text-align": "center"
};

const data = {
  labels: ["Business", "Clients", "other"],
  datasets: [
    {
      data: [300, 50, 100],
      backgroundColor: ["#FF6384", "#36A2EB", "#FFCE56"],
      hoverBackgroundColor: ["#FF6384", "#36A2EB", "#FFCE56"]
    }
  ]
};

class DoughnutExample extends Component {
  render() {
    return (
      <div>
        <h2 style={headingStyle}>Cash Flow</h2>
        <Doughnut data={data} id="ch" />
      </div>
    );
  }
}

export default DoughnutExample;
